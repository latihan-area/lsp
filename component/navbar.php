<div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Inventaris</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">IV</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Menu</li>
            <li>
              <a href="home.php" class="nav-link"><i class="fas fa-fire"></i><span>Home</span></a>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Barang</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="peminjaman.php">Peminjaman</a></li>
                <?php if($_SESSION['nama_level'] == 'Admin'){ ?>
                <li><a class="nav-link" href="stok.php">Stok</a></li>
                <?php } ?>
              </ul>
            </li>
            <?php if($_SESSION['nama_level'] == 'Admin'){ ?>
            <li>
              <a href="inventaris.php" class="nav-link"><i class="fas fa-fire"></i><span>Inventaris</span></a>
            </li>
            <li>
              <a href="jenis.php" class="nav-link"><i class="fas fa-fire"></i><span>Jenis</span></a>
            </li>
            <li>
              <a href="ruang.php" class="nav-link"><i class="fas fa-fire"></i><span>Ruang</span></a>
            </li>
            <li>
              <a href="laporan.php" class="nav-link"><i class="fas fa-fire"></i><span>Laporan</span></a>
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>User</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="petugas.php">Petugas</a></li>
                <li><a class="nav-link" href="peminjam.php">Peminjam</a></li>
                <li><a class="nav-link" href="pegawai.php">Pegawai</a></li>
              </ul>
            </li>
            <?php } ?>
            <li>
              <a href="profile.php" class="nav-link"><i class="fas fa-fire"></i><span>Profile</span></a>
            </li>
          </ul>

        </aside>
      </div>