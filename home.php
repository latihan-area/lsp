<?php

    //koneksi database
    require("config/koneksi.php");
    //memulai session
    session_start();

    //cek session petugas
    if(!isset($_SESSION['username_petugas']) && !isset($_SESSION['password_petugas'])){
        //jika ada akan dialihkan ke halaman home
        header("location:login.php");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Ecommerce Dashboard &mdash; Stisla</title>
  <?php include("component/css.php"); ?>
</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <?php include("component/header.php"); ?>
      <?php include("component/navbar.php"); ?>
      <!-- Main Content -->
      <div class="main-content">
        
      </div>
      <?php include("component/footer.php"); ?>
    </div>
  </div>

  <?php include("component/js.php"); ?>
</body>
</html>

