<?php

    //koneksi database
    require("config/koneksi.php");
    //memulai session
    session_start();

    //cek session petugas
    if(isset($_SESSION['username_petugas']) && isset($_SESSION['password_petugas'])){
        //jika ada akan dialihkan ke halaman home
        header("location:home.php");
    } else {
        //jika tidak ada akan dialihkan ke halaman login
        header("location:login.php");
    }

