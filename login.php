<?php

    //koneksi database
    require("config/koneksi.php");
    //memulai session
    session_start();

    //cek session petugas
    if(isset($_SESSION['username_petugas']) && isset($_SESSION['password_petugas'])){
        //jika ada akan dialihkan ke halaman home
        header("location:home.php");
    }

    if(isset($_POST['login'])){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $sql = mysqli_query($db, "SELECT * FROM petugas,pegawai,level WHERE petugas.username_petugas='$username' AND petugas.password_petugas='$password' AND pegawai.id_pegawai=petugas.id_pegawai AND level.id_level=petugas.id_level");

        if(mysqli_num_rows($sql) == 1){
            $row = mysqli_fetch_array($sql);

            session_start();

            //session dari tabel pegawai
            $_SESSION['id_pegawai'] = $row['id_pegawai'];
            $_SESSION['nama_pegawai'] = $row['nama_pegawai'];
            $_SESSION['nip_pegawai'] = $row['nip_pegawai'];
            $_SESSION['alamat_pegawai'] = $row['alamat_pegawai'];

            //session dari tabel petugas
            $_SESSION['id_petugas'] = $row['id_petugas'];
            $_SESSION['username_petugas'] = $row['username_petugas'];
            $_SESSION['password_petugas'] = $row['password_petugas'];

            //session dari tabel level
            $_SESSION['id_level'] = $row['id_level'];
            $_SESSION['nama_level'] = $row['nama_level'];
            
            header("location:home.php");
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login &mdash; Stisla</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="assets/modules/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/components.css">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <!-- /END GA -->
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              <img src="assets/img/stisla-fill.svg" alt="logo" width="100" class="shadow-light rounded-circle">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Login</h4></div>

              <div class="card-body">
                <form method="POST" action="login.php" class="needs-validation" novalidate="">
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
                    <div class="invalid-feedback">
                      Please fill in your username
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="d-block">
                    	<label for="password" class="control-label">Password</label>
                    </div>
                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>

                  <div class="form-group">
                    <input type="submit" name="login" value="Masuk" class="btn btn-primary btn-lg btn-block"/>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; Bimo Rio Prastiawan 2019
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="assets/modules/jquery.min.js"></script>
  <script src="assets/modules/popper.js"></script>
  <script src="assets/modules/tooltip.js"></script>
  <script src="assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="assets/modules/moment.min.js"></script>
  <script src="assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->

  <!-- Page Specific JS File -->
  
  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/custom.js"></script>
</body>
</html>
