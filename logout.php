<?php
    //memulai session
    session_start();
    //menghapus session
    session_destroy();

    //mengalihkan ke halaman index
    header("location:index.php");

?>